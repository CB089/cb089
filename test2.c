#include <stdio.h>
int main() {
	int hour, minute, timeinmin;
	printf("Enter the value for hour:");
	scanf("%d", &hour);
	printf("Enter the value for minute:");
	scanf("%d", &minute);
	timeinmin = minute + (hour * 60);
	printf("Total minutes in %02dH:%02dM is %d\n",
		   hour, minute, timeinmin);
	return 0;
}
